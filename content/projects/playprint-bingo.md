---
title: Playprint Bingo
subtitle: An Android app
date: 2016-06-15
tags: ["android", "java", "mysql"]
---
This gaming app was developed with the aim of licencing it for use to bingo halls across Ireland and the UK.

It contains four different game modes: Bingo, Raffle, Lotto and Wheel of Fortune. Each game is fully customisable to suit a variety of playstyles.

It is currently in use at over 20 locations and sees repeat use at party bingo events.

{{< youtube IpTP_b7lFW8 >}}