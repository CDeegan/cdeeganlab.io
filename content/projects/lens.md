---
title: Lens
subtitle: An Android app
date: 2017-11-09
tags: ["android", "java", "firebase", "augmented reality"]
---
Lens is an Android app that was developed as my final year project at university. It was awarded the highest possible grade.

It acts as a location-based social sharing platform with augmented reality capabilities. Firebase is used for user authentication and data storage. The augmented reality functionality was implemented without the use of any third-party libraries.

This project contains a full suite of specification and testing documents, which are available upon request.

{{< youtube HPX-0huo5sA >}}