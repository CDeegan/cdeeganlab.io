---
title: Steam Trade Bot
subtitle: An Steam trading bot
date: 2015-09-20
tags: ["c#", "mysql", "steam"]
---

This bot was developed to buy virtual items at discount rates. This allowed me to sell these items at a real-world cash profit.

This project was forked from an existing [Steam Trading Bot repository](https://github.com/Jessecar96/SteamBot). Functionality was extended with the following features:

 - read pricing data from a MySQL database
 - trade queue to manage multiple concurrent users
 - item stock limits to prevent over-purchasing
 - chat commands to initiate trades or to indicate bot status
