---
title: Swaps.tf
subtitle: An Steam trading web app
date: 2017-12-12
tags: ["nodejs", "angularjs", "mongodb", "socket.io", "steam"]
---
The first iteration of swaps.tf, this web app was written in AngularJS and interfaced with a NodeJS Express web server and a NodeJS Steam bot instance.

{{< youtube rxMtFshizrI >}}