---
title: Swaps.tf 2.0
subtitle: An Steam trading web app
date: 2020-01-18
tags: ["nodejs", "vuejs", "mongodb", "redis", "socket.io", "steam", "docker", "docker-compose"]
---

swaps.tf was rebuilt from the ground up, with a focus on improved software development practices and some additional features that would draw in more users.

#### Client
The front-end web app is written in VueJS and uses Vuetify as a stylistic framework. Vuex is used to handle state management, which is integrated with a socket.io listener.

#### Web Server
The web server is built using Express and includes a robust API that deals with users, inventories, authentication, gateway transactions and more. Redis is used to store session data and as a message bus for communicating with Steam bot instances. A socket service is used to transmit data to any connected clients, in real time.

#### Steam Bot App
This NodeJS app spins up a number of Steam bot instances and handles incoming messages in the Redis message queue. Each Steam bot instance is capable of managing and sending its own trades.

#### Deployment
swaps.tf is hosted on a Debian DigitalOcean droplet. Using Docker, each service runs in a container and Docker Compose is used to manage this group of containers. SSL certificates are generated at regular intervals using LetsEncrypt.

All code repositiories for this project make use of Gitlab's CI pipelines. For all code pushes, a Docker image is built and pushed to the swaps.tf server.


{{< youtube YcItzgv_E3M >}}